# Drupal-Talan

## Name
Talan recrutement project.

## Description
Content types administration is driving by the users' region.

## Installation
```bash
# Clone project with SSH.
git clone git@gitlab.com:BereniceLecocq/drupal-talan.git
# Go to project folder.
cd drupal-talan
# Launch DDEV (For DDEV installation, see https://ddev.readthedocs.io/en/stable/users/install/ddev-installation/#linux).
## You can change project name for DDEV if you needed, here : .ddev/config.yaml
ddev start
## Need : Uncomment lines at 865 to 867 in file /web/sites/default/settings.php !
# Install project dependencies.
ddev composer install
# Install default Drupal database.
ddev import-db --file=dumps/database-20240618-19H34.sql
# Install Drupal configuration.
ddev drush cim
# Clear cache of Drupal.
ddev drush cr
```
## Usage
```bash
# You can import Drupal database with content.
ddev import-db --file=dumps/database-20240618-22H17.sql
# Install Drupal configuration.
ddev drush cim
# Clear cache of Drupal.
ddev drush cr
# You can login with :
## Centre Admin
## Sud Admin
## Nord Admin
## Password : Azerty123456!
## On https://talan.ddev.site/user
## For Super Admin : ddev drush uli
```

